module _figureDrawer {
    requires javafx.fxml;
    requires javafx.controls;
    requires log4j;
    requires javafx.swing;
    requires javafx.graphics;
    requires java.base;

    opens com.stormnet.figuresfx.controller;
    opens com.stormnet.figuresfx;
}