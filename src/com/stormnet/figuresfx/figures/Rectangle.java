package com.stormnet.figuresfx.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.Objects;

public class Rectangle extends Figure {
    private double height;
    private double width;

    public Rectangle(double cX, double cY, double lineWidth, Color color) {
        super(Figure.FIGURE_TYPE_RECTANGLE, cX, cY, lineWidth, color);
    }

    public Rectangle(double cX, double cY, double lineWidth, Color color, double height, double width) {
        this(cX, cY, lineWidth, color);
        this.height = height < 10 ? 10 : height;
        this.width = width < 10 ? 10 : width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rectangle rectangle = (Rectangle) o;
        return Double.compare(rectangle.height, height) == 0 &&
                Double.compare(rectangle.width, width) == 0;
    }

    @Override
    public int hashCode() {

        return Objects.hash(height, width);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Rectangle{");
        sb.append("height=").append(height);
        sb.append(", width=").append(width);
        sb.append(", cX=").append(cX);
        sb.append(", cY=").append(cY);
        sb.append(", lineWidth=").append(lineWidth);
        sb.append(", color=").append(color);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setLineWidth(lineWidth);
        gc.setStroke(color);
        gc.strokeRect(cX - width / 2, cY - height / 2, width, height);
    }
}
